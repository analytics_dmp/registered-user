SELECT
    auth.AUTH_UID as ssoid,
    Date(auth.AUTH_REGISTER_DATE_TIMESTAMP) as register_date,
    upper(k.app_type) as platform,
    case 
          when app_id in (1,2,29,73,171,175,205,206,207,212,213,317, 305,306) then 'DMP'
          else 'NON-DMP'
    end as DMP_APP,
    case
          when app_id = 1 then 'True Life web'
          when app_id = 2 then 'TrueselfService'
          when app_id = 29 then 'TrueID Web'
          when app_id = 73 then 'iService app'
          when app_id = 171 then 'iService web'
          when app_id = 175 then 'Trueyou'
          when app_id = 205 then 'True Music iOS'
          when app_id = 206 then 'True Music Android'
          when app_id = 207 then 'True Music web'
          when app_id = 212 then 'TrueID Android'
          when app_id = 213 then 'TrueID iOS'
          when app_id = 317 then 'TrueTV web'
          when app_id = 305 then 'TrueTV iOS'
          when app_id = 306 then 'TrueTV Android'
          else 'NON_DMP'
        end as doc_name_dmp
        ,case
          when app_id = 1 then 'True Life'
          when app_id = 2 then 'TrueselfService'
          when app_id = 29 then 'TrueID'
          when app_id = 73 then 'iService'
          when app_id = 171 then 'iService'
          when app_id = 175 then 'Trueyou'
          when app_id = 205 then 'TrueMusic'
          when app_id = 206 then 'TrueMusic'
          when app_id = 207 then 'TrueMusic'
          when app_id = 212 then 'TrueID'
          when app_id = 213 then 'TrueID'
          when app_id = 317 then 'TrueTV'
          when app_id = 305 then 'TrueTV'
          when app_id = 306 then 'TrueTV'
          else k.app_name
        end as application_name
FROM [true-dmp:BI_ANALYTICS.authentication] as auth
LEFT JOIN 
( SELECT app_id, app_name, app_type
FROM [true-dmp:BI_ANALYTICS.MAST_APPID]
) as K
on auth.AUTH_REGISTER_BY_APP = K.app_id
where AUTH_STATUS = 1
and REGEXP_MATCH(STRING(AUTH_UID ),r"^\d+$")