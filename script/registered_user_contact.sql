select 
ssoid
,auth_account
,contact_number
,email
,cit_id
,register_year
,register_month
,regist_dt
,regist_by_app
,register_channel
,application_name
,max(last_login_dt) as last_login_dt
from
(
select 
ssoid
,auth_account
,concat('0',string(contact_number)) as contact_number
,email
,cit_id
,register_year
,register_month
,regist_dt
,last_login_dt
,regist_by_app
,register_channel 
,application_name
from (
select 
     ssoid
    ,auth_account
    ,case when (CAST(t1 as integer) is not null) and ( CAST(t1 as integer) between 100000000 and 999999999)  then CAST(t1 as integer) else (case when (CAST(t2 as integer) between 100000000 and 999999999) and t2 is not null then CAST(t2 as integer)  else null end)  end as contact_number 
    ,case when (CAST(t1 as string) is not null) and (cast(t1 as integer) is  null) then cast(t1 as string) else (case when (CAST(t2 as string) is not null) and (cast(t2 as integer) is null) then CAST(t2 as string)  else null end)  end as email
    ,case when (CAST(t2 as integer) is not null) and (CAST(t2 as integer) between 1000000000000 and 9999999999999)  then CAST(t2 as integer) else (case when (CAST(t3 as integer) between 1000000000000 and 9999999999999) and t3 is not null then CAST(t3 as integer)  else null end)  end as cit_id
    ,register_year
    ,register_month
    ,regist_dt
    ,last_login_dt
    ,regist_by_app
    ,register_channel
    ,application_name
from (
select 
        string(ssoid) as ssoid
        ,auth_account
        ,first(split(auth_account ,'::'))t1
        ,nth(2,split(auth_account ,'::'))t2
        ,nth(3,split(auth_account ,'::'))t3
        ,nth(4,split(auth_account ,'::'))t4
        ,register_year
        ,register_month
        ,regist_dt
        ,last_login_dt
        ,regist_by_app 
        ,register_channel
        ,application_name
from (
SELECT 
    auth_uid as ssoid
    ,auth_account
    ,year(AUTH_REGISTER_DATE_TIMESTAMP) as register_year
    ,month(AUTH_REGISTER_DATE_TIMESTAMP) as register_month
    ,AUTH_REGISTER_DATE_TIMESTAMP  as regist_dt
    ,DATE(FORMAT_UTC_USEC((INTEGER(AUTH_LAST_LOGIN)+25200)*1000000))  as last_login_dt
    ,AUTH_REGISTER_BY_APP as regist_by_app 
    , case
          when AUTH_REGISTER_BY_APP = 1 then 'True Life web'
          when AUTH_REGISTER_BY_APP = 2 then 'TrueselfService'
          when AUTH_REGISTER_BY_APP = 29 then 'TrueID Web'
          when AUTH_REGISTER_BY_APP = 73 then 'iService app'
          when AUTH_REGISTER_BY_APP = 171 then 'iService web'
          when AUTH_REGISTER_BY_APP = 175 then 'Trueyou'
          when AUTH_REGISTER_BY_APP = 205 then 'True Music iOS'
          when AUTH_REGISTER_BY_APP = 206 then 'True Music Android'
          when AUTH_REGISTER_BY_APP = 207 then 'True Music web'
          when AUTH_REGISTER_BY_APP = 212 then 'TrueID Android'
          when AUTH_REGISTER_BY_APP = 213 then 'TrueID iOS'
          when AUTH_REGISTER_BY_APP = 317 then 'TrueTV web'
          when AUTH_REGISTER_BY_APP = 305 then 'TrueTV iOS'
          when AUTH_REGISTER_BY_APP = 306 then 'TrueTV Android'
      end as register_channel
      ,case
          when AUTH_REGISTER_BY_APP = 1 then 'True Life'
          when AUTH_REGISTER_BY_APP = 2 then 'TrueselfService'
          when AUTH_REGISTER_BY_APP = 29 then 'TrueID'
          when AUTH_REGISTER_BY_APP = 73 then 'iService'
          when AUTH_REGISTER_BY_APP = 171 then 'iService'
          when AUTH_REGISTER_BY_APP = 175 then 'Trueyou'
          when AUTH_REGISTER_BY_APP = 205 then 'TrueMusic'
          when AUTH_REGISTER_BY_APP = 206 then 'TrueMusic'
          when AUTH_REGISTER_BY_APP = 207 then 'TrueMusic'
          when AUTH_REGISTER_BY_APP = 212 then 'TrueID'
          when AUTH_REGISTER_BY_APP = 213 then 'TrueID'
          when AUTH_REGISTER_BY_APP = 317 then 'TrueTV'
          when AUTH_REGISTER_BY_APP = 305 then 'TrueTV'
          when AUTH_REGISTER_BY_APP = 306 then 'TrueTV'
        end as application_name
FROM [true-dmp:BI_ANALYTICS.authentication] 
where auth_status=1
and  AUTH_REGISTER_BY_APP in (1,2,29,73,171,175,205,206,207,212,213,317,305,306)
group by 1,2,3,4,5,6,7,8,9
order by 3 desc
limit 1000
)
)
group by 1,2,3,4,5,6,7,8,9,10,11,12
)
)
group by 1,2,3,4,5,6,7,8,9,10,11